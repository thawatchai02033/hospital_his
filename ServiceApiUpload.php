<?php
header('Content-Type: application/json; charset=utf-8');
//header('Content-Type: multipart/form-data; charset=utf-8');
//var_dump($_FILES["file"]);
//$target_dir = "uploads/";
//$target_file = $target_dir . basename($_FILES["file"]["name"]);
//$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
//// Check if image file is a actual image or fake image
//$check = getimagesize($_FILES["file"]["tmp_name"]);
//if ($check !== false) {
//    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
//        echo "The file ". htmlspecialchars( basename( $_FILES["file"]["name"])). " has been uploaded.";
//    } else {
//        echo "Sorry, there was an error uploading your file.";
//    }
//} else {
//    echo "File is not an image.";
//}


if(!isset($_FILES["file"])){
    echo json_encode(array(
        'statusCode' => 403,
        'message' => 'input file not found'
    ));
    return;
}

$file = $_FILES["file"]["tmp_name"];
$ext = explode("/", $_FILES["file"]['type'])[1];
$file_name = md5_file($file);
$name = $file_name.'.'.$ext;
if(array_search($ext, array('jpg','png','bmp','gif','jpeg','pdf')) == false){
    echo json_encode(array(
        'statusCode' => 403,
        'message' => 'extension file not support'
    ));
    return;
}

//$path = "/mnt/DATA/html/HR/rest/files/";
$path = "uploads/";
$datePath = $path.date('Y/m/d').'/';
if(!is_dir($datePath)){
    mkdir($path.date('Y').'/');
    sleep(1);
    mkdir($path.date('Y/m').'/');
    sleep(1);
    mkdir($datePath);
    sleep(1);
}

$dirfile = $datePath . $name;
if(move_uploaded_file($file, $dirfile)){
//    $url = 'https://medhr.medicine.psu.ac.th/HospitalCardRegister/uploads/'.date('Y/m/d/').$name;
    $url = '/'.date('Y/m/d/').$name;
    $key = base64_encode(date('Y-m-d-').$file_name.' '.$ext);
    echo json_encode(array(
        'statusCode' => 200,
        'url' => $url,
        'key' => $key
    ));
}else{
    echo json_encode(array(
        'statusCode' => 500,
        'message' => 'uploaded fail'
    ));
}