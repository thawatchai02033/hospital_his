export default {
  getProvinces (state) {
    return state.provinces
  },
  getAmphur (state) {
    return state.amphur
  },
  getTumbon (state) {
    return state.tumbon
  },
  getSmsResult (state) {
    return state.smsResult
  },
  getOtpData (state) {
    return state.otpData
  },
  getHnData (state) {
    return state.hnData
  },
  getReligionData (state) {
    return state.religion
  },
  getRelevantData (state) {
    return state.relevant
  },
  getOccupationData (state) {
    return state.occup
  },
  getStatusData (state) {
    return state.status
  },
  getIpData (state) {
    return state.ip
  },
  getMessageErr (state) {
    return state.message
  },
  getRequestData (state) {
    return state.request
  },
  getRequestBystatusData (state) {
    return state.requestByStatus
  },
  getFileData (state) {
    return state.file
  },
  getEgovData (state) {
    return state.Egov
  },
  getLogApproveData (state) {
    return state.LogApprove
  },
  getMeData(state) {
    return state.MeData
  },
  getHideContent(state) {
    return state.HideContent
  },
  getSmsLog(state) {
    return state.SmsLog
  },
  getRequesttStatusData(state) {
    return state.RequestStatusMs
  },
  getCountPerson(state) {
    return state.countPerson
  },
}
