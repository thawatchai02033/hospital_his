export default {
  setProvince(state, data) {
    state.provinces = []
    if (data.length >= 0) {
      state.provinces = data
    } else {
      state.provinces.push(data)
    }
  },
  setAmphur(state, data) {
    state.amphur = []
    if (data.length >= 0) {
      state.amphur = data
    } else {
      state.amphur.push(data)
    }
  },
  setTumbon(state, data) {
    state.tumbon = []
    if (data.length >= 0) {
      state.tumbon = data
    } else {
      state.tumbon.push(data)
    }
  },
  setOtpData(state, data) {
    state.otpData = []
    if (data.length >= 0) {
      state.otpData = data
    } else {
      state.otpData.push(data)
    }
  },
  setHnData(state, data) {
    state.hnData = []
    if (data.length >= 0) {
      state.hnData = data
    } else {
      state.hnData.push(data)
    }
  },
  setReligionData(state, data) {
    state.religion = []
    if (data.length >= 0) {
      state.religion = data
    } else {
      state.religion.push(data)
    }
  },
  setRelevantData(state, data) {
    state.relevant = []
    if (data.length >= 0) {
      state.relevant = data
    } else {
      state.relevant.push(data)
    }
  },
  setOccupationData(state, data) {
    state.occup = []
    if (data.length >= 0) {
      state.occup = data
    } else {
      state.occup.push(data)
    }
  },
  setStatusData(state, data) {
    state.status = []
    if (data.length >= 0) {
      state.status = data
    } else {
      state.status.push(data)
    }
  },
  setSendSms(state, data) {
    state.smsResult = data
  },
  setIp(state, data) {
    state.ip = data
  },
  setMessage(state, data){
    state.message = []
    if (data.length >= 0) {
      state.message = data
    } else {
      state.message.push(data)
    }
  },
  setRequest(state, data){
    state.request = []
    if (data.length >= 0) {
      state.request = data
    } else {
      state.request.push(data)
    }
  },
  setRequestByStatus(state, data){
    state.requestByStatus = []
    if (data.length >= 0) {
      state.requestByStatus = data
    } else {
      state.requestByStatus.push(data)
    }
  },
  setEgovData(state, data){
    state.Egov = []
    if (data.length >= 0) {
      state.Egov = data
    } else {
      state.Egov.push(data)
    }
  },
  setLogApprove(state, data){
    state.LogApprove = []
    if (data.length >= 0) {
      state.LogApprove = data
    } else {
      state.LogApprove.push(data)
    }
  },
  setFileData(state, data){
    state.file = null
    if (data != null) {
      state.file = data
    }
  },
  setSmsLog(state, data){
    state.SmsLog = null
    if (data != null) {
      state.SmsLog = data
    }
  },
  setCountPerson(state, data){
    state.countPerson = null
    if (data != null && data.length > 0) {
      state.countPerson = data[0].count
    }
  },
  SET_ME_DATA(state, data) {
    state.MeData = []
    if (data.length >= 0) {
      state.MeData = data
      // state.MeData[0].perid = '42706'
    } else {
      state.MeData.push(data)
      // state.MeData[0].perid = '42706'
    }
  },
  SET_HIDE_CONTENT(state, data) {
    state.HideContent = data
  },
  SET_RQEUEST_STATUS_DATA(state, data) {
    state.RequestStatusMs = data
  },
}
