import Const from '../../Const/Const'

export default {
  initPermiss ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'staffData',
        param: {
          response: data.token,
        }
      }).then((res) => {
        if (res.data.status) {
          commit('SET_ME_DATA', res.data.staff)
          resolve(true)
        } else {
          // commit('setMessage', res.data.result)
          commit('SET_ME_DATA', res.data.staff)
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          commit('SET_ME_DATA', [])
          resolve(false)
        }
      })
    })
  },
  initPermiss2 ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'staffData',
        param: {
          response: data.token,
        }
      }).then((res) => {
        if (res.data.status) {
          commit('SET_ME_DATA', res.data.staff)
          resolve(true)
        } else {
          // commit('setMessage', res.data.result)
          commit('SET_ME_DATA', res.data.staff)
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          commit('SET_ME_DATA', [])
          document.cookie = 'vurl=' + Const.hospitalUrl
          window.location.href = Const.medhrUrl + '/SingleSign/'
        }
      })
    })
  },
  initProvincesData ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios(Const.medhrUrl2 + 'stdMed/Changwat/?limit=9999').then((res) => {
        if (res.data) {
          commit('setProvince', res.data)
          resolve(true)
        } else {
          commit('setProvince', [])
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          commit('setProvince', [])
          resolve(false)
        }
      })
    })
  },
  initAmphurData ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios(Const.medhrUrl2 + 'stdMed/Famphur/?limit=9999').then((res) => {
        if (res.data) {
          commit('setAmphur', res.data)
          resolve(true)
        } else {
          commit('setAmphur', [])
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          commit('setAmphur', [])
          resolve(false)
        }
      })
    })
  },
  initTumbonData ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios(Const.medhrUrl2 + 'stdMed/Ftumbon/?limit=9999').then((res) => {
        if (res.data) {
          commit('setTumbon', res.data)
          resolve(true)
        } else {
          commit('setTumbon', [])
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          commit('setTumbon', [])
          resolve(false)
        }
      })
    })
  },
  initRequestData ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'getAllRequest',
        param: {
          response: data.token,
        }
      }).then((res) => {
        if (res.data) {
          commit('setRequest', res.data)
          resolve(true)
        } else {
          // commit('setMessage', res.data.result)
          commit('setRequest', [])
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          commit('setRequest', [])
          resolve(false)
        }
      })
    })
  },
  initRequestDataByStatus ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'getAllRequestByStatus',
        param: {
          status_id: '1',
          response: data.token,
        }
      }).then((res) => {
        if (res.data) {
          commit('setRequestByStatus', res.data)
          resolve(true)
        } else {
          // commit('setMessage', res.data.result)
          commit('setRequestByStatus', [])
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          commit('setRequestByStatus', [])
          resolve(false)
        }
      })
    })
  },
  CheckRequestData ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'getRequest',
        param: {
          ...data,
          response: data.token,
        }
      }).then((res) => {
        if (res.data) {
          commit('setRequest', res.data)
          resolve(true)
        } else {
          // commit('setMessage', res.data.result)
          commit('setRequest', [])
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          commit('setRequest', [])
          resolve(false)
        }
      })
    })
  },
  CheckHnOrRequestData ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'checkHnOrRequest',
        param: btoa(JSON.stringify({
          ...data,
          response: data.token,
        }))
      }).then((res) => {
        if (res.data) {
          commit('setRequest', res.data)
          resolve(true)
        } else {
          // commit('setMessage', res.data.result)
          commit('setRequest', [])
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          commit('setRequest', [])
          resolve(false)
        }
      })
    })
  },
  updateRequestData ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'updateRequest',
        param: {
          ...data,
          response: data.token,
        }
      }).then((res) => {
        if (res.data.status) {
          resolve(true)
        } else {
          // commit('setMessage', res.data.result)
          resolve(false)
        }
      }).catch(err => {
        if (err) {
          /*this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
            queryName: 'saveLogMessage',
            param: {
              ...data,
              message: err.message,
              response: data.token,
            }
          })*/
          resolve(false)
        }
      })
    })
  },
  updateRequestFormGroverment ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'updateRequestFormGroverment',
        param: {
          ...data,
          response: data.token,
        }
      }).then((res) => {
        if (res.data.status) {
          resolve(true)
        } else {
          // commit('setMessage', res.data.result)
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  updateRequestStatus ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'updateRequestStatus',
        param: {
          ...data,
          response: data.token,
        }
      }).then((res) => {
        if (res.data.status) {
          resolve(true)
        } else {
          // commit('setMessage', res.data.result)
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  deleteRequest ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'deleteRequest',
        param: {
          ...data,
          response: data.token,
        }
      }).then((res) => {
        if (res.data.status) {
          resolve(true)
        } else {
          // commit('setMessage', res.data.result)
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  logApprove ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'logApprove',
        param: {
          ...data,
          response: data.token,
        }
      }).then((res) => {
        if (res.data.status) {
          resolve(true)
        } else {
          // commit('setMessage', res.data.result)
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  initLogApprove ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'getLogApprove',
        param: {
          ...data,
          response: data.token,
        }
      }).then((res) => {
        if (res.data) {
          commit('setLogApprove', res.data)
          resolve(true)
        } else {
          commit('setLogApprove', [])
          // commit('setMessage', res.data.result)
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          commit('setLogApprove', [])
          resolve(false)
        }
      })
    })
  },
  initMultiLogApprove ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'getMultiLogApprove',
        param: {
          ...data,
          response: data.token,
        }
      }).then((res) => {
        if (res.data) {
          commit('setLogApprove', res.data)
          resolve(true)
        } else {
          commit('setLogApprove', [])
          // commit('setMessage', res.data.result)
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          commit('setLogApprove', [])
          resolve(false)
        }
      })
    })
  },
  CheckSmsData ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'checkSms',
        param: {
          ...data,
          response: data.token,
        }
      }).then((res) => {
        if (res.data.status) {
          resolve(true)
        } else {
          // commit('setMessage', res.data.result)
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  CheckOtpSmsData ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'checkOTP',
        param: {
          ...data,
          response: data.token,
        }
      }).then((res) => {
        if (res.data.status) {
          resolve(true)
        } else {
          // commit('setMessage', res.data.result)
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })

    // return new Promise((resolve) => {
    //   this.$axios.get(Const.medhrUrl2 + 'hrtime/sms_sender/person_id/' + data.person_id + '/otp/' + data.otp + '/active/1').then((res) => {
    //     if (res.data) {
    //       commit('setOtpData', res.data)
    //       resolve(true)
    //     } else {
    //       resolve(false)
    //     }
    //   }).catch(res => {
    //     if (res) {
    //       resolve(false)
    //     }
    //   })
    // })
  },
  CheckOtpSmsData2 ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'checkOTP2',
        param: {
          ...data,
          response: data.token,
        }
      }).then((res) => {
        if (res.data.status) {
          resolve(true)
        } else {
          // commit('setMessage', res.data.result)
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  SaveSmsData ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl2 + 'hrtime/sms_sender', {
        person_id: data.person_id,
        phone_number: data.phoneNumber,
        brithday: data.brithday,
        otp: data.otp,
        system_name: data.system_name,
      }, {
        headers: {
          'X-REST-METHOD': 'PUT',
          'Content-Type': 'application/json'
        }
      }).then((res) => {
        if (res.data.status) {
          resolve(true)
        } else {
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  UpdateSmsData ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl2 + 'hrtime/sms_sender/' + data.url, data.smsdata, {
        headers: {
          'Content-Type': 'application/json'
        }
      }).then((res) => {
        if (res.data.status) {
          resolve(true)
        } else {
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  SendSms ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'sendSms',
        param: {
          tar_num: data.phoneNumber.indexOf(0) == 0 ? '66' + data.phoneNumber.substr(1, data.phoneNumber.length) : data.phoneNumber,
          tar_msg: data.message,
          response: data.token
        }
      }).then((res) => {
        if (res.data.status) {
          if (res.data.result.statusCode == '200') {
            resolve(true)
          } else {
            resolve(false)
          }
        } else {
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  sendSmsMessage ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'sendSmsMessage',
        param: {
          phone_number: data.phone_number,
          message: data.message,
          response: data.token
        }
      }).then((res) => {
        if (res.data.status) {
          if (res.data.result.statusCode == '200') {
            resolve(true)
          } else {
            resolve(false)
          }
        } else {
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  getSmsLogData ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'getSmsLog',
        param: {
          id: data.request_id,
          response: data.token
        }
      }).then((res) => {
        console.log(res.data)
        if (res.data) {
          commit('setSmsLog', res.data)
          resolve(true)
        } else {
          commit('setSmsLog', [])
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          commit('setSmsLog', [])
          resolve(false)
        }
      })
    })
  },
  sendLineNotify ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'sendLineNotify',
        param: {
          full_Name: data.full_Name,
          response: data.token
        }
      }).then((res) => {
        if (res.data.status) {
          if (res.data.result.statusCode == '200') {
            resolve(true)
          } else {
            resolve(false)
          }
        } else {
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  insertSmsMessageCustom ({ commit }, data) {
    return new Promise((resolve) => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'insertSmsMessageCustom',
        param: {
          id: data.request_id,
          phone: data.phone,
          message: data.message,
          perid: data.perid,
          response: data.token
        }
      }).then((res) => {
        if (res.data.status) {
          resolve(true)
        } else {
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  CheckHnData ({ commit }, data) {
    return new Promise((resolve) => {
      // this.$axios.get(Const.his01Url + 'gPID2HN.php?ID13=' + data.person_id).then(res => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'dataPatient',
        param: {
          ...data,
          response: data.token
        }
      }).then((res) => {
        if (res.data) {
          commit('setHnData', res.data)
          resolve(true)
        } else {
          commit('setHnData', [])
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          commit('setHnData', [])
          resolve(false)
        }
      })
    })
  },
  CheckHnData2 ({ commit }, data) {
    return new Promise((resolve) => {
      // this.$axios.get(Const.his01Url + 'gPID2HN.php?ID13=' + data.person_id).then(res => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'checkHnForHis',
        param: btoa(JSON.stringify({
          ...data,
          response: data.token
        }))
      }).then((res) => {
        if (res.data.status) {
          commit('setHnData', res.data.data)
          resolve(true)
        } else {
          commit('setHnData', [])
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          commit('setHnData', [])
          resolve(false)
        }
      })
    })
  },
  initReligionData ({ commit }) {
    return new Promise((resolve) => {
      // this.$axios.get(Const.his01Url + 'gPID2HN.php?ID13=' + data.person_id).then(res => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'getDataReligionNew',
        param: {}
      }).then((res) => {
        if (res.data) {
          commit('setReligionData', res.data)
          resolve(true)
        } else {
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  initRelevantData ({ commit }) {
    return new Promise((resolve) => {
      // this.$axios.get(Const.his01Url + 'gPID2HN.php?ID13=' + data.person_id).then(res => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'getDataRelativeNew',
        param: {}
      }).then((res) => {
        if (res.data) {
          commit('setRelevantData', res.data)
          resolve(true)
        } else {
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  initOccupData ({ commit }) {
    return new Promise((resolve) => {
      // this.$axios.get(Const.his01Url + 'gPID2HN.php?ID13=' + data.person_id).then(res => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'getDataOccupationNew',
        param: {}
      }).then((res) => {
        if (res.data) {
          commit('setOccupationData', res.data)
          resolve(true)
        } else {
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  initStatusData ({ commit }) {
    return new Promise((resolve) => {
      // this.$axios.get(Const.his01Url + 'gPID2HN.php?ID13=' + data.person_id).then(res => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'getDataStatusNew',
        param: {}
      }).then((res) => {
        if (res.data) {
          commit('setStatusData', res.data)
          resolve(true)
        } else {
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  initStatusRequestData ({ commit }) {
    return new Promise((resolve) => {
      // this.$axios.get(Const.his01Url + 'gPID2HN.php?ID13=' + data.person_id).then(res => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'getDataStatusRequest',
        param: {}
      }).then((res) => {
        if (res.data) {
          commit('SET_RQEUEST_STATUS_DATA', res.data)
          resolve(true)
        } else {
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  uploadFile ({ commit }, data) {
    return new Promise((resolve) => {
      // this.$axios.get(Const.his01Url + 'gPID2HN.php?ID13=' + data.person_id).then(res => {
      const formData = new FormData()
      formData.append('file', data.files)
      const headers = { 'Content-Type': 'multipart/form-data' }
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApiUpload.php', formData, headers).then((res) => {
        if (res.data.statusCode == '200') {
          commit('setFileData', res.data)
          resolve(true)
        } else {
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  checkTokenCaptcha ({ commit }, data) {
    return new Promise((resolve) => {
      // this.$axios.get(Const.his01Url + 'gPID2HN.php?ID13=' + data.person_id).then(res => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'checkToken',
        param: {
          response: data.token,
        }
      }).then((res) => {
        if (res.data.status) {
          if (res.data.result.success) {
            resolve(true)
          } else {
            resolve(false)
          }
        } else {
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  initEgovData ({ commit }, data) {
    return new Promise((resolve) => {
      // this.$axios.get(Const.his01Url + 'gPID2HN.php?ID13=' + data.person_id).then(res => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'getEgovData',
        param: {
          ServiceID: data.ServiceID,
          CitizenID: data.CitizenID,
          response: data.token
        }
      }).then((res) => {
        if (res.data.status) {
          if (res.data.data != null) {
            commit('setEgovData', res.data)
            resolve(true)
          } else {
            resolve(false)
          }
        } else {
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  },
  saveCountPerson ({ commit }, data) {
    return new Promise((resolve) => {
      // this.$axios.get(Const.his01Url + 'gPID2HN.php?ID13=' + data.person_id).then(res => {
      this.$axios.post(Const.medhrUrl + 'HospitalCardRegister/ServiceApi.php', {
        queryName: 'saveCountPerson',
        param: {}
      }).then((res) => {
        if (res.data) {
          commit('setCountPerson', res.data)
          resolve(true)
        } else {
          resolve(false)
        }
      }).catch(res => {
        if (res) {
          resolve(false)
        }
      })
    })
  }
}

function parseJwt (token) {
  var base64Url = token.split('.')[1]
  var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
  var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
  }).join(''))
  // console.log(JSON.parse(jsonPayload))
  // return {perid: '47758'}
  return JSON.parse(jsonPayload)
}

function getCookie (cname) {
  var name = cname + '='
  var ca = document.cookie.split(';')
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i]
    while (c.charAt(0) == ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length)
    }
  }
  return ''
}
