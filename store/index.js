import State from './person_reg/state'
import Mutations from './person_reg/mutations2'
import Actions from './person_reg/actions'
import Getters from './person_reg/getters'

export const state = State

export const mutations = Mutations

export const actions = Actions

export const getters = Getters
