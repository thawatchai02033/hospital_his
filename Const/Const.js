export default {
  // medhrUrl: 'https://medhr.medicine.psu.ac.th/HospitalCard/v1/hospReg/',
  // medhrUrl2: 'https://medhr.medicine.psu.ac.th/app-api/v2/?/apis/',
  medhrUrl: process.env.NODE_ENV === 'development'? '/MedHr/' : 'https://medhr.medicine.psu.ac.th/',
  medhrUrl2: process.env.NODE_ENV === 'development'? '/MedHr/app-api/v2/?/apis/' : 'https://medhr.medicine.psu.ac.th/app-api/v2/?/apis/',
  medhrV1: process.env.NODE_ENV === 'development'? '/MedHr/rest/v1/' : 'https://medhr.medicine.psu.ac.th/rest/v1/',
  his01Url: process.env.NODE_ENV === 'development'? '/his01/his/' : 'https://his01.psu.ac.th/his/',
  his01stdUrl: process.env.NODE_ENV === 'development'? '/his01/' : 'https://his01.psu.ac.th/',
  // hospitalUrl: process.env.NODE_ENV === 'development'? 'http://localhost:8090/' : 'https://medhr.medicine.psu.ac.th/HospitalCardRegisterDev',
  hospitalUrl: process.env.NODE_ENV === 'development'? 'http://localhost:8090/' : 'https://medhr.medicine.psu.ac.th/HospitalCardRegister',
}
