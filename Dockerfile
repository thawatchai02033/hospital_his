FROM nginx:1.27-alpine

COPY ./dist /usr/share/nginx/html
COPY saveNewPatient.php /usr/share/nginx/html
COPY saveNewPatient2.php /usr/share/nginx/html
COPY ServiceApi.php /usr/share/nginx/html
COPY ServiceApiUpload.php /usr/share/nginx/html
COPY checkSession.php /usr/share/nginx/html

EXPOSE 80
