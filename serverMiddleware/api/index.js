const express = require('express')
const app = express()
// app.set('trust proxy', true)

app.get('/', function(req, res) {
  // res.send('ip: ' + req.socket.remoteAddress )
  console.log('this is middleware')
  res.json({
    ip: req.socket.remoteAddress
  })
})

app.get('/Test', function(req, res, next) {
  // res.send('ip: ' + req.socket.remoteAddress )
  console.log('this is middleware')
  res.json({
    ip: req.socket.remoteAddress
  })
  next()
})

module.exports = { path: '/api/', handler: app }
