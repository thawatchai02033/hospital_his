import colors from 'vuetify/es5/util/colors'

export default {
  // https://nuxtjs.org/docs/configuration-glossary/configuration-mode/
  // mode: 'spa',
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: process.server ? true : false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - ระบบทำบัตรโรงพยาบาลสงขลานครินทร์ออนไลน์',
    title: 'ระบบทำบัตรโรงพยาบาลสงขลานครินทร์ออนไลน์',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: ''
      },
      {
        name: 'format-detection',
        content: 'telephone=no'
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        // href: process.env.NODE_ENV === 'development' ? '/favicon.ico' : '/HospitalCardRegisterDev/favicon.ico',
        href: process.env.NODE_ENV === 'development' ? '/favicon.ico' : '/HospitalCardRegister/favicon.ico'
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['~/plugins/filters.js'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/recaptcha',
    'nuxt-sweetalert2',
    '@nuxtjs/proxy',
    'vue-web-cam/nuxt'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/',
    // credentials: true,
    proxy: true
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  recaptcha: {
    /* reCAPTCHA options */
    hideBadge: true,
    siteKey: process.env.NODE_ENV === 'development' ? '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI' : '6LeWM88eAAAAAAPw5mGc4DV6PI-BBqfX0zK4CrSm',
    version: 3
  },
  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  // build: {
  //   publicPath: '_nuxt/dist/'
  //   // publicPath: 'https://cdn.nuxtjs.org'
  // },
  target: 'static', // or 'static' : 'server'
  serverMiddleware: [
    // '~/serverMiddleware/api/index.js'
  ],
  router: {
    // base: process.env.NODE_ENV === 'development' ? '/' : '/HospitalCardRegisterDev/',
    base: process.env.NODE_ENV === 'development' ? '/' : '/HospitalCardRegister/',
    middleware: [],

  },
  // generate: {
  //   routes: ['/']
  //   // dir: 'https://medhr.medicine.psu.ac.th/HospitalCardRegisterDev'
  // },
  // static: {
  //   prefix: false
  // }
  // buildDir: '_nuxt/dist/',
  publicPath: './',
  ...process.env.NODE_ENV === 'development' && {
    proxy: {
      '/MedHr': 'https://medhr.medicine.psu.ac.th',
    }
  },
  proxyTable: {
    '/MedHr': {
      target: 'https://medhr.medicine.psu.ac.th',
      changeOrigin: true,
      pathRewrite: {
        '^/MedHr': ''
      }
    }
  },
  devServer: {
    proxy: {
      '/MedHr': {
        target: 'https://medhr.medicine.psu.ac.th',
        changeOrigin: true,
        secure: false,
        pathRewrite: { '^/MedHr': '' },
        logLevel: 'debug'
      },
      '/MedNet': {
        target: 'https://mednet.psu.ac.th',
        changeOrigin: true,
        secure: false,
        pathRewrite: { '^/MedNet': '' },
        logLevel: 'debug'
      }
    }
  },
  proxy: {
    '/MedHr': {
      target: 'https://medhr.medicine.psu.ac.th',
      changeOrigin: true,
      secure: false,
      pathRewrite: { '^/MedHr': '' },
      logLevel: 'debug'
    },
    '/his01': {
      target: 'https://his01.psu.ac.th',
      changeOrigin: true,
      secure: false,
      pathRewrite: { '^/his01': '' },
      logLevel: 'debug'
    }
    /*    // Proxies /foo to http://example.com/foo
        'http://example.com/foo',

        // Proxies /api/books/!*!/!**.json to http://example.com:8000
        'http://example.com:8000/api/books/!*!/!**.json',

        // You can also pass more options
        [ 'http://example.com/foo', { ws: false } ]*/
  },
  // env: {
  //   baseURL: process.env.BASE_URL
  // }
}
