import Const from '~/Const/Const'

export default function ({ store, $axios }) {
  $axios.get(Const.hospitalUrl + 'api').then(res => {
    store.commit('setIp', res.data.ip)
  })
}
