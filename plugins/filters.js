
import Vue from 'vue'

Vue.filter('BrithDayTh', (value) => {
  if (value == '' || value == null)
    return null
  var year = value.split('-')[0]
  var month = value.split('-')[1]
  var day = value.split('-')[2]
  return (parseInt(year) + 543) + '-' + month + '-' + day
})

Vue.filter('dateToThai', (value) => {
  var DateTrans = "";
  if (value != undefined) {
    var Day = value.split('-')[2];
    var Month = value.split('-')[1];
    var Year = value.split('-')[0];
    var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
      'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
    ];
    DateTrans = (parseInt(Month) == 0 ? 'ไม่ทราบ' : Day.split(' ')[0]) + " " + (parseInt(Month) == 0 ? 'ไม่ทราบ' : monthNames[parseInt(Month) - 1]) + " " +
      (parseInt(Year) + 543)
  }
  return (
    DateTrans
  );
})

Vue.filter('dateToThai2', (value) => {
  var DateTrans = "";
  if (value != undefined) {
    var date = value.split(' ')[0]
    var time = value.split(' ')[1]
    var Day = date.split('-')[2];
    var Month = date.split('-')[1];
    var Year = date.split('-')[0];
    var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
      'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
    ];
    DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
      (parseInt(Year) + 543) + ' ' + time + ' น.'
  }
  return (
    DateTrans
  );
})

Vue.filter('dateEgovToThai', (value) => {
  var DateTrans = "";
  value = '' + value
  if (value != undefined) {
    var Day = value.substring(6);
    var Month = value.substring(4,6);
    var Year = value.substring(0,4);
    var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
      'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
    ];
    DateTrans = Day.split(' ')[0] + " " + monthNames[parseInt(Month) - 1] + " " +
      (parseInt(Year))
  }
  return (
    DateTrans
  );
})

Vue.filter('dateEgovToDB', (value) => {
  var DateTrans = "";
  value = '' + value
  if (value != undefined) {
    var Day = value.substring(6);
    var Month = value.substring(4,6);
    var Year = value.substring(0,4);
    DateTrans = (parseInt(Year) - 543) + "-" + Month + "-" + Day
  }
  return (
    DateTrans
  );
})


